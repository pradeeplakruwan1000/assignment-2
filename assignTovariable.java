 // Testing.
 private void invertSomeDominoes() {
    for (Domino d : _d) {
	boolean isRandom = Math.random() > 0.5;
      if (isRandom) {
        d.invert();
      }
    }
  }
