  private void generate(boolean isDominoes) {
    _d = new LinkedList<Domino>();
    int count = 0;
    int x = 0;
    int y = 0;
    for (int l = 0; l <= 6; l++) {
      for (int h = l; h <= 6; h++) {
        Domino d = new Domino(h, l);
        _d.add(d);
	  count++;
	  if(isDominoes) {
        	d.place(x, y, x + 1, y);
        	x += 2;
        	if (x > 6) {
          		x = 0;
          		y++;
        	}
       }
      	}
    }
    if (count != 28) {
      System.out.println("something went wrong generating dominoes");
      System.exit(0);
    }
  }


private void generateDominoes() {
    generate(true);
}

