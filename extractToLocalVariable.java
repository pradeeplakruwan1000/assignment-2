  private Domino findDominoAt(int x, int y) {
    for (Domino d : _d) {
	    boolean isLx = d.lx == x && d.ly == y;
        boolean isLy = d.hx == x && d.hy == y;
        if (isLx || isLy) {
            return d;
        }
    }
    return null;
  }
