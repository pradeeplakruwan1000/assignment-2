  private void shuffleDominoesOrder() {
    final List<Domino> shuffled = new LinkedList<Domino>();

    while (_d.size() > 0) {
      final int n = (int) (Math.random() * _d.size());
      shuffled.add(_d.get(n));
      _d.remove(n);
    }

    _d = shuffled;
  }
