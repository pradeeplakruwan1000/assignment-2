  private void tryToRotateDominoAt(int x, int y) {
    if (thisIsTopLeftOfDomino(x, y, findDominoAt(x, y))) {
      if (d.ishl()) {
        boolean weFancyARotation = Math.random() < 0.5;
        if (weFancyARotation) {
          if (theCellBelowIsTopLeftOfHorizontalDomino(x, y)) {
            Domino e = findDominoAt(x, y + 1);
            e.hx = x;
            e.lx = x;
            d.hx = x + 1;
            d.lx = x + 1;          }
        }
      }
    }
  }
